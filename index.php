<?php require_once "./code.php"
	// Adding internal script here

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S3: Classess, Objects, Inheritance and Polymorphism</title>
</head>
<body>

	<h1>Objects from Variable</h1>

	<p><?php echo $buildingObj -> name; ?></p>

	<!-- Start of Object from Classes -->

	<h1>Objects From Classes</h1>

	<p><?php var_dump($building); ?></p>
	<p><?php print_r($building);?></p>

	<h1>Inheritance and Polymorphism</h1>

	<p><?php print_r($building->printName());?></p>
	<p><?php echo $building->printName();?></p>

	<p><?php echo $condominium->printName(); ?></p>




	<h1>Access Modifiers</h1>

	<h2>Building Variables</h2>
	<p><?php //echo $building->name; ?></p>

	<h2>Condominium Variables</h2>
	<p><?php //echo $condominium->name; ?></p>


	<h1>Encapsulation</h1>
	<p>The name of the condominium is 
		<?php echo $condominium->getName(); ?>

	</p>

	<p>
		
		<?php $condominium->setName('Enzo Tower');?>

	</p>

	<p>The name of the condo has been changed to <?php echo $condominium->getName(); ?></p>


	<h1>S3 Activity</h1>

	<h3>Person</h3>
	<p>
		Person: <?php echo $person->printName(); ?>
	</p>

	<h3>Developer</h3>

	<p>
		Developer: <?php echo $developer->printName(); ?>
	</p>

	<h3>Engineer</h3>

	<p>
		Engineer: <?php echo $engineer->printName(); ?>		
	</p>

</body>
</html>